
import java.util.Scanner;

public class PCHaendler {
	private static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		// Benutzereingaben lesen
		String artikel = liesString("Was möchten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettopreis = nettoGesamtPreis(anzahl, preis);
		double bruttopreis = bruttoGesamtPreis(nettopreis, mwst);

		// Ausgeben
		rechnungsausgeben(artikel, anzahl, nettopreis, bruttopreis, mwst);

	}

	static String liesString(String text) {
		System.out.println(text);
		return myScanner.next();
	}

	static int liesInt(String text) {
		System.out.println(text);
		return myScanner.nextInt();
	}

	static double liesDouble(String text) {
		System.out.println(text);
		return myScanner.nextDouble();
	}

	static double nettoGesamtPreis(int anzahl, double nettopreis) {
		return (anzahl * nettopreis);
	}

	static double bruttoGesamtPreis(double nettopreis, double mwst) {
		return (nettopreis * (1 + mwst / 100));
	}

	static void rechnungsausgeben(String artikel, int anzahl, double nettopreis, double bruttopreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettopreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttopreis, mwst, "%");
	}
}