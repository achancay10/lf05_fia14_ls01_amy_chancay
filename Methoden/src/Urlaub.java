
import java.util.Scanner;

public class Urlaub {

	public static void main(String[] args) {
		Currency wechsel = null;
		Scanner reise = new Scanner(System.in);
		System.out.println("Wie hoch ist dein Budget? ");
		double budget = reise.nextDouble();
		while (budget >= 100) {
			System.out.println("In welches Land reist du als nächstes?");
			String ziel = reise.next();

			System.out.println("Einzuwechselden Betrag in Euro:");
			double inputInEuro = reise.nextDouble();
			if (inputInEuro > budget) {
				System.out.println("So viel Geld hast du nicht :(");
				continue;
			}
			if (inputInEuro < 0) {
				System.out.println("Wie bitte?");
				continue;
			}

			if (ziel.equalsIgnoreCase("usa")) {
				wechsel = convertEurToUsd(inputInEuro);

			} else if (ziel.equalsIgnoreCase("japan")) {
				wechsel = convertEurToYen(inputInEuro);

			} else if (ziel.equalsIgnoreCase("england")) {
				wechsel = convertEurToPfund(inputInEuro);

			} else if (ziel.equalsIgnoreCase("schweiz")) {
				wechsel = convertEurToCHF(inputInEuro);

			} else if (ziel.equalsIgnoreCase("schweden")) {
				wechsel = convertEurToSEK(inputInEuro);

			} else {
				System.out.println("Da können wir noch nicht hin");
				continue;
			}

			System.out.println("Du bekommst " + String.format("%.2f", wechsel.getValue()) + " " + wechsel.getName());
			budget = budget - inputInEuro;
			System.out.println("Dein Budget ist: " + budget + " Euro.");
		}
		System.out.println("Zuhause jetzt");
	}

	private static Currency convertEurToSEK(double euro) {
		return new Currency(euro * 10.10, "SEK");
	}

	private static Currency convertEurToCHF(double euro) {
		return new Currency(euro * 1.08, "CHF");
	}

	private static Currency convertEurToPfund(double euro) {
		return new Currency(euro * 0.89, "GBP");
	}

	private static Currency convertEurToYen(double euro) {
		return new Currency(euro * 126.50, "JPY");
	}

	public static Currency convertEurToUsd(double euro) {
		return new Currency(euro * 1.22, "USD");
	}

}
