
public class WeltDerZahlen {

	public static void main(String[] args) {
	// Die Anzahl der Planeten in unserem Sonnesystem   
		short anzahlPlaneten = 8;
		
	//Anzahl der Sterne in unserer Milchstraße
		int anzahlSterne = 200;
		
	// Wie viele Einwohner hat Berlin?
		double bewohnerBerlin = 3.775480;
		
	//Wie alt bist du?  Wie viele Tage sind das?
		int alterTage = 9490;
		
	// Wie viel wiegt das schwerste Tier der Welt?
	   // Schreiben Sie das Gewicht in Kilogramm auf!
		int gewichtKilogramm = 150000;
		
	// Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
		long flaecheGroessteLand = 17098242;
		
	// Wie groß ist das kleinste Land der Erde?
		double flaecheKleinsteLand = 0.44;
		
		System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
		System.out.println("Anzahl der Sterne: " + anzahlSterne);
		System.out.println("Einwohner in Berlin: " + bewohnerBerlin);
		System.out.println("Wie Tage alt bin ich: " + alterTage);
		System.out.println("Schwerste Tier der Welt: " + gewichtKilogramm + "Kg");
		System.out.println("Größte Land der Erde: " + flaecheGroessteLand + "Km²");
		System.out.println("Kleinste Land der Erde: " + flaecheKleinsteLand + "Km²");
		System.out.println("******* Ende des Programms *******");

	}

}
