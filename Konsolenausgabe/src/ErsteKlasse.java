
public class ErsteKlasse {

	public static void main(String[] args) {
		System.out.println(" Fahrenheit |  Celsius ");
		String l = "-----------------------";
		System.out.printf("%18s\n", l);
		double f1 = -20;
		double c1= -28.8889;
		System.out.printf("%-12.2f|%10.2f\n", f1, c1);
		double f2 = -10;
		double c2= -23.3333;
		System.out.printf("%-12.2f|%10.2f\n", f2, c2);
		double f3 = +0;
		double c3= -17.7778;
		System.out.printf("%-12.0f|%10.2f\n", f3, c3);
		double f4 = +20;
		double c4= -6.6667;
		System.out.printf("%-12.0f|%10.2f\n", f4, c4);
		double f5 = +30;
		double c5= -1.1111;
		System.out.printf("%-12.0f|%10.2f\n", f5, c5);
		
	}

}
